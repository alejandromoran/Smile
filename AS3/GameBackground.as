﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.display.DisplayObject;
	
	public class GameBackground extends MovieClip {
		
		private var scene : Object;
		private var screenDirection : Boolean = false;
		public function GameBackground(scene) {
			// constructor code
			this.scene = scene;
			scene.addChild(this);
			this.x = 0;
			this.y = 0;
			this.addEventListener(Event.ENTER_FRAME, EnterFrameHandler);
			this.score.text = "0";
		}
		public function UpdateScore(score : uint) : void
		{
			this.score.text = score.toString();
		}
		private function EnterFrameHandler(e:Event)
		{
			if ((this.x + this.bg.width) > 400 && !screenDirection)
			{
				this.bg.x -= 0.5;
			}
			if ((this.bg.x + this.bg.width) < 400 && !screenDirection)
			{
				screenDirection = true;
			}
			
			if (this.bg.x < 0 && screenDirection)
			{
				this.bg.x += 0.5;
			}
			if (this.bg.x >= 0 && screenDirection)
			{
				screenDirection = false;
			}
			
			
		}
	}
	
}
