﻿package 
{
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.display.DisplayObject;

	public class GameLogic extends MovieClip
	{
		private var stopToPlayStatus:Boolean;
		private var score:uint = 0;
		private var enemyTimer:Timer;
		private var levelScoreTop:uint = 0;
		private var levelTime:uint = 0;
		private var player:Player;
		private var gameBg:GameBackground;
		private var scene:Object;
		private var speed : uint;
		public function GameLogic(scene)
		{
			// constructor code
			addEventListener(Event.ENTER_FRAME, EnterFrameHandler);
			this.scene = scene;
			gameBg = new GameBackground(scene);
			player = new Player(scene);
			levelScoreTop = 5;
			levelTime = 1000;
			speed = 0.5;
			enemyTimer = new Timer(levelTime);
			enemyTimer.addEventListener(TimerEvent.TIMER, TimerCompleteHandler);
			enemyTimer.start();
		}
		private function AddScore():void
		{
			score +=  1;
			gameBg.UpdateScore(score);
			trace(score);
		}
		public function GetScore():uint
		{
			return score;
		}
		private function EnterFrameHandler(e:Event):void
		{
			for (var i : int = 0; i < scene.numChildren; i++)
			{
				if (scene.getChildAt(i) is Enemy)
				{
					if (scene.getChildAt(i).y >= 590)
					{
						AddScore();
						return;
					}
					var enemy:MovieClip = scene.getChildAt(i);
					for (var k : int = 0; k < enemy.numChildren; k++)
					{
						if (enemy.getChildAt(k) is EnemyCollisionPoint)
						{
							for (var w:int = 0; w < player.numChildren; w++)
							{
								if (player.getChildAt(w) is PlayerCollisionPoint)
								{
									if (enemy.getChildAt(k).hitTestObject(player.getChildAt(w)))
									{
										enemyTimer.removeEventListener(TimerEvent.TIMER, TimerCompleteHandler);

										scene.removeChild(gameBg);
										scene.removeChild(player);
										for (var u : int = 0; u < scene.numChildren; u++)
										{
											if (scene.getChildAt(u) is Enemy)
											{
												Enemy(scene.getChildAt(u)).Destroy();
												trace(u);
												u--;
											}
										}
										
										stopToPlayStatus = true;
										return;
									}
								}
							}
						}
					}
				}
			}
		}
		
		private function TimerCompleteHandler(e:TimerEvent)
		{
			if (score >= levelScoreTop)
			{
				levelScoreTop = score + 10;
				levelTime -=  50;
				enemyTimer.delay = levelTime;
				speed += 1;
			}
			new Enemy(scene, speed);
		}
		public function StopToPlay():Boolean
		{
			return stopToPlayStatus;
		}
	}

}