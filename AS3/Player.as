﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	
	public class Player extends MovieClip {
		
		private var right : Boolean;
		private var left : Boolean;
		
		public function Player(scene) {
			// constructor code
			scene.addChild(this);
			this.x = 200;
			this.y = 600 - this.height;
			scene.addEventListener(KeyboardEvent.KEY_DOWN, KeyDownHandler);
			scene.addEventListener(KeyboardEvent.KEY_UP, KeyUpHandler);
			scene.addEventListener(Event.ENTER_FRAME, EnterFrameHandler);
			for (var i:int = 0; i < this.numChildren; i++)
			{
				if (this.getChildAt(i) is PlayerCollisionPoint)
				{
					this.getChildAt(i).visible = false;
				}
			}
		}
		private function EnterFrameHandler(e:Event)
		{
			if (right && this.x + this.width < 400)
			{
				this.x += 10;
			}
			if (left && this.x > 0)
			{
				this.x -= 10;
			}
		}
		public function KeyDownHandler(event:KeyboardEvent):void
		{
			if (event.keyCode == 37)
			{
				left = true;
			}
			if (event.keyCode == 39)
			{
				right = true;
			}
		}
		function KeyUpHandler(event:KeyboardEvent):void
		{
			if (event.keyCode == 37)
			{
				left = false;
			}
			if (event.keyCode == 39)
			{
				right = false;
			}
		}
	}
	
}
