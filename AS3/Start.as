﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	
	public class Start extends MovieClip {
		
		private var startToPlayStatus : Boolean;
		public function Start(scene) {
			// constructor code
			scene.addChild(this);
			startGame.addEventListener(MouseEvent.CLICK, StartGameHandler);
		}
		private function StartGameHandler(e:MouseEvent) : void
		{
			startToPlayStatus = true;
		}
		public function StartToPlay() : Boolean
		{
			return startToPlayStatus;
		}
		public function GetUsername() : String
		{
			return username.text;
		}
		
	}
	
}
