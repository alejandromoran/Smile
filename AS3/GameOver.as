﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	public class GameOver extends MovieClip {
		
		private var playAgainStatus : Boolean;
		public function GameOver(scene, totalScore : uint) {
			// constructor code
			scene.addChild(this);
			this.score.text = totalScore.toString();
			this.playAgain.addEventListener(MouseEvent.CLICK, PlayAgainHandler);
		}
		private function PlayAgainHandler (e:MouseEvent) : void
		{
			playAgainStatus = true;
			trace("coño " + playAgainStatus);
		}
		public function PlayAgain() : Boolean
		{
			return playAgainStatus;
		}
	}
	
}
