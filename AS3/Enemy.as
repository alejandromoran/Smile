﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.display.DisplayObject;
	
	public class Enemy extends MovieClip {
		
		private var scene;
		private var speed;
		public function Enemy(scene, speed) {
			// constructor code
			this.scene = scene;
			this.speed = speed + Math.random()*3 ;
			scene.addChild(this);
			this.x = Math.round(Math.random()*350);
			this.y = - this.height;
			this.addEventListener(Event.ENTER_FRAME, EnterFrameHandler);
			for (var i:int = 0; i < this.numChildren; i++)
			{
				if (this.getChildAt(i) is EnemyCollisionPoint)
				{
					this.getChildAt(i).visible = false;
				}
			}
		}
		private function EnterFrameHandler(e:Event)
		{
			this.y += (4 + speed);
			if (this.y >= 600)
			{
				Destroy();
			}
		}
		public function Destroy() : void
		{
			this.removeEventListener(Event.ENTER_FRAME, EnterFrameHandler);
			scene.removeChild(this);
		}
	}
	
}
