package com.tatianitasmile;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class LevelSelectScreen implements Screen{
	
	private Game game;
	private TextButton easy, medium, hard;
	private Stage stage;
	private int screenWidth, screenHeight;
	private IActivityRequestHandler myRequestHandler;
	
	public LevelSelectScreen(Game game,IActivityRequestHandler handler)
	{
		this.game = game;
		screenWidth = Assets.getScreenWidth();
		screenHeight = Assets.getScreenHeight();
		myRequestHandler = handler;	
	}

	public void draw(float deltaTime)
	{

	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(26, 114, 151, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {	
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		Table table = new Table(Assets.skin);
		
		// UI Buttons
		easy = new TextButton("Easy", Assets.skin);
		medium = new TextButton("Medium", Assets.skin);
		hard = new TextButton("Hard", Assets.skin);
		
		Image backImage = new Image(Assets.mainMenuScreenBackground);
		backImage.setSize(screenHeight, screenHeight);

		easy.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				clearScreen();
				game.setScreen(new GameScreen(game,1,myRequestHandler));
				return true;
			}

		});

		medium.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				clearScreen();
				game.setScreen(new GameScreen(game,2,myRequestHandler));
				return true;
			}

		});

		hard.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				clearScreen();
				game.setScreen(new GameScreen(game,3,myRequestHandler));
				return true;
			}

		});
		
		

		
		table.setFillParent(true);
		
		table.add(easy).width(150).height(50).padBottom(1);
		table.row();
		table.add(medium).width(150).height(50).padBottom(1).center();
		table.row();
		table.add(hard).width(150).height(50).padBottom(1);
		
		backImage.setPosition(screenWidth/2 - backImage.getWidth()/2, screenHeight/2 - backImage.getHeight()/2);
		stage.addActor(backImage);
		stage.addActor(table);
		
	}
	
	private void clearScreen()
	{
		stage.clear();
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}
