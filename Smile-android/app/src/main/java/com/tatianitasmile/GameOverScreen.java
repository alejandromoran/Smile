package com.tatianitasmile;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;


public class GameOverScreen implements Screen {

	private Game game;
	private Stage stage;
	private Label lbl;
	private TextButton playAgain, mainMenu, exitGame;
	private com.tatianitasmile.IActivityRequestHandler myRequestHandler;
	private int level;
	private int score;
	
	public GameOverScreen (Game game, int level, com.tatianitasmile.IActivityRequestHandler handler, int score)
	{
		this.game = game;
		this.score = score;
		this.level = level;
		myRequestHandler = handler;
		myRequestHandler.showAds(true);
		//SwarmLeaderboard.submitScore(5326, (float) score);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		playAgain = new TextButton("Play again", com.tatianitasmile.Assets.skin);
		mainMenu = new TextButton("Menu", com.tatianitasmile.Assets.skin);

		Table table = new Table(com.tatianitasmile.Assets.skin);
		Gdx.input.setInputProcessor(stage);
		
		playAgain.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				// TODO Auto-generated method stub
				clearScreen();
				game.setScreen(new com.tatianitasmile.GameScreen(game,level, myRequestHandler));
				return true;
			}

		});

		mainMenu.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{
				clearScreen();
				game.setScreen(new com.tatianitasmile.MainMenuScreen(game, myRequestHandler));
				return true;
			}

		});
		

		table.setFillParent(true);
		String lblText = "You've got " + score + " points";
		lbl = new Label(lblText, com.tatianitasmile.Assets.skin);
		
		table.add(lbl).padBottom(15);
		table.row();
		table.add(playAgain).width(150).height(50).padBottom(1);
		table.row();
		table.add(mainMenu).width(150).height(50).padBottom(1);
		table.row();
		table.add(exitGame).width(150).height(50).padBottom(1);

		stage.addActor(table);
	}
	
	private void clearScreen()
	{
		stage.clear();
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
