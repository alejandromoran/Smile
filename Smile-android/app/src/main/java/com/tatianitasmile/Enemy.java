package com.tatianitasmile;

import com.badlogic.gdx.math.Circle;

public class Enemy extends Circle {

	private static final long serialVersionUID = 4452941336424155849L;
	private int id;
	private float speed;
	
	public Enemy(float x, float y, float radius)
	{
		super(x,y,radius);
	}
	
	public float getSpeed()
	{
		return speed;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setSpeed(float speed)
	{
		this.speed = speed;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
}
