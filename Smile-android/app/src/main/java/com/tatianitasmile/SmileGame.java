package com.tatianitasmile;

import com.badlogic.gdx.Game;

public class SmileGame extends Game {

	private IActivityRequestHandler myRequestHandler;

	public SmileGame(IActivityRequestHandler handler) {
		 myRequestHandler = handler;
	}
	
	@Override
	public void create() {
		Assets.load();
		setScreen(new MainMenuScreen(this, myRequestHandler));	
	}
}
