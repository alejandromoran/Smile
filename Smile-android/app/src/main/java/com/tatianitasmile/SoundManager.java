package com.tatianitasmile;

public class SoundManager {
	public static void playMusic()
	{	
		if (Assets.getLastMusicStatus())
		{
			Assets.bso.setLooping(true);
			Assets.bso.play();
		}
	}
	public static void stopMusic()
	{
		if (Assets.getLastMusicStatus())
		{
			Assets.bso.stop();
		}
	}
	public static void changeMusicStatus()
	{
		if (Assets.getLastMusicStatus())
		{
			loadMusic();
			return;
		}
		else
		{
			disposeMusic();
			return;
		}
	}
	public static void disposeMusic()
	{
		Assets.bso.dispose();
	}
	public static void loadMusic()
	{
		Assets.loadMusic();
	}
}
