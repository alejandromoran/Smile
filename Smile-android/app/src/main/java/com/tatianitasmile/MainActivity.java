package com.tatianitasmile;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

import com.tatianitasmile.IActivityRequestHandler;
import com.tatianitasmile.SmileGame;

public class MainActivity extends AndroidApplication implements IActivityRequestHandler {
	
	 private static final int MY_APP_D = 3332;
	 private static final String MY_APP_K = "39f50c4c378045de3af324dbb1b30dba";
	 protected AdView adView;
	 private final int SHOW_ADS = 1;
	 private final int HIDE_ADS = 0;

	 protected Handler handler = new Handler()
	 {
		 @Override
		 public void handleMessage(Message msg) {
			 switch(msg.what) {
			 	case SHOW_ADS:
	            {
	            	adView.setVisibility(View.VISIBLE);
	            	break;
	            }
	         	case HIDE_ADS:
	         	{
	         		adView.setVisibility(View.GONE);
	         		break;
	         	}
			 }
		 }
	 }; 
	 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        cfg.useCompass = false;
        cfg.useAccelerometer = false;
      
        // Create the layout
        RelativeLayout layout = new RelativeLayout(this);

        // Do the stuff that initialize() would do for you
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        // Create the libgdx View
        View gameView = initializeForView(new SmileGame(this), cfg); 
        
        // Create and setup the AdMob view
        adView = new AdView(this, AdSize.BANNER, "d2de42e6833548f1"); // Put in your secret key here
        adView.loadAd(new AdRequest());

        // Add the libgdx view
        layout.addView(gameView);

        // Add the AdMob view
        RelativeLayout.LayoutParams adParams = 
                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, 
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        layout.addView(adView, adParams);

        // Hook it all up
        setContentView(layout);    
        // Add this method call
        //Swarm.setActive(this);
    }

	@Override
	public void showAds(boolean show) {
		// TODO Auto-generated method stub
		handler.sendEmptyMessage(show ? SHOW_ADS : HIDE_ADS);
	}
	
	public void onResume()
	{
		super.onResume();
	    //Swarm.setActive(this);
	    //Swarm.init(this, MY_APP_D, MY_APP_K);
	}
	public void onPause()
	{
	    super.onPause();
	    //Swarm.setInactive(this);
	}
	public void onStop()
	{
	    super.onStop();
	    //Swarm.setInactive(this);
	}
	public void onDestroy()
	{
	    super.onDestroy();
	    //Swarm.setInactive(this);
	}

}