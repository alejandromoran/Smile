package com.tatianitasmile;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

public class MainMenuScreen implements Screen{
	
	private Game game;
	private TextButton startGame, musicControl, highScores, exitGame;
	private TextField username;
	private Stage stage;
	private String musicStatus;
	private int screenWidth, screenHeight;
	private IActivityRequestHandler myRequestHandler;
	
	public MainMenuScreen(Game game,IActivityRequestHandler handler)
	{
		this.game = game;
		screenWidth = Assets.getScreenWidth();
		screenHeight = Assets.getScreenHeight();
		myRequestHandler = handler;	
	}

	public void draw(float deltaTime)
	{

	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(26, 114, 151, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		
		updateMusicStatus();
		
		
		stage = new Stage();
		Gdx.input.setInputProcessor(stage);
		
		Table table = new Table(Assets.skin);
		
		// UI Buttons
		startGame = new TextButton("Play", Assets.skin);
		highScores = new TextButton("High Scores", Assets.skin);
		musicControl = new TextButton("Music " + musicStatus, Assets.skin);
		exitGame = new TextButton("Exit", Assets.skin);
		Image backImage = new Image(Assets.mainMenuScreenBackground);
		backImage.setSize(screenHeight, screenHeight);

		startGame.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				clearScreen();
				game.setScreen(new LevelSelectScreen(game,myRequestHandler));
				return true;
			}

		});

		highScores.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				//Swarm.showLeaderboards();
				return true;
			}

		});
		
		musicControl.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y,
					int pointer, int button) {
				Assets.switchMusicControl();
				SoundManager.changeMusicStatus();
				updateMusicStatus();
				musicControl.setText("Music " + musicStatus);
				return true;
			}

		});
		
		exitGame.addListener(new InputListener() {

			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
			{			
				//Swarm.logOut();
				System.exit(0);
				return true;
			}

		});
		
		table.setFillParent(true);
		
		table.add(username).width(150).height(50).padBottom(1);
		table.row();
		table.add(startGame).width(150).height(50).padBottom(1).center();
		table.row();
		table.add(musicControl).width(150).height(50).padBottom(1);
		table.row();
		table.add(highScores).width(150).height(50).padBottom(1);
		table.row();
		table.add(exitGame).width(150).height(50).padBottom(1).padBottom(1);
		
		backImage.setPosition(screenWidth/2 - backImage.getWidth()/2, screenHeight/2 - backImage.getHeight()/2);
		stage.addActor(backImage);
		stage.addActor(table);
		
	}
	
	private void updateMusicStatus()
	{
		if (Assets.getLastMusicStatus())
		{
			musicStatus = "ON";
		}
		else
		{
			musicStatus = "OFF";
		}
	}
	
	private void clearScreen()
	{
		stage.clear();
	}
	
	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
}
