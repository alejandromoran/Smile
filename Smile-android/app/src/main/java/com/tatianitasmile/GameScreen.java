package com.tatianitasmile;

import android.content.ClipData;

import java.util.Iterator;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class GameScreen implements Screen {

	private Game game;
	private Array<com.tatianitasmile.Enemy> enemies;
	private Array<Item> items;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Circle player;
	private long lastDropTime, spawnTime;
	private float backgroundXPosition = 0;
	private boolean changeDirection = false;
	private Label lbl;
	private Stage stage;
	private Table table;
	private int score, screenWidth, screenHeight;
	private com.tatianitasmile.IActivityRequestHandler myRequestHandler;
	private int enemyNumber;
	private int bombsDropped;
	private int level;
	// It's 64/480 because is the correct resolution I started to work with
	private final double widthRatio = 0.13; 
	
	private int playerWidth;
	
	public GameScreen(Game game,int level, com.tatianitasmile.IActivityRequestHandler handler)
	{
		this.game = game;
		this.level = level;
		score = enemyNumber = bombsDropped =0;
		spawnTime = 425000000;
		myRequestHandler = handler;
		myRequestHandler.showAds(false);
		screenWidth = com.tatianitasmile.Assets.getScreenWidth();
		screenHeight = com.tatianitasmile.Assets.getScreenHeight();
		enemies = new Array<com.tatianitasmile.Enemy>();
		items = new Array<Item>();
		camera = new OrthographicCamera();
		camera.setToOrtho(false,screenWidth,screenHeight);
		batch = new SpriteBatch();
		playerWidth = getWidthWithRatio();
		player = new Circle((screenWidth/2)-playerWidth, 0, playerWidth/2);
		com.tatianitasmile.SoundManager.playMusic();
		//spawnEnemy();
		if(level == 1)
		{
			spawnTime += 425000000;
		}
		if(level == 2)
		{
			spawnTime += 225000000;
		}
	}
	
	private int getWidthWithRatio() {
		return (int) (widthRatio * screenWidth);
	}

	private void spawnEnemy()
	{
		com.tatianitasmile.Enemy enemy = new com.tatianitasmile.Enemy(MathUtils.random(0, screenWidth-playerWidth),screenHeight,playerWidth/2);
		enemy.setId(enemyNumber);
		if(level==1)
		{
			enemy.setSpeed(MathUtils.random(100, 200));
		}
		if(level==2)
		{
			enemy.setSpeed(MathUtils.random(150, 350));
		}
		if(level==3)
		{
			enemy.setSpeed(MathUtils.random(200, 400));	
		}
		
		enemies.add(enemy);
		lastDropTime = TimeUtils.nanoTime();
	}
	
	private void dropBomb()
	{
		Bomb bomb = new Bomb(MathUtils.random(0, screenWidth-playerWidth),screenHeight,playerWidth/2);
		bomb.setId(bombsDropped);
		bomb.setSpeed(MathUtils.random(200, 400));
		items.add(bomb);
		bombsDropped++;
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();	
		batch.draw(Assets.gameScreenBackground,backgroundXPosition,0,screenWidth + (screenHeight/2),screenHeight);
		batch.draw(Assets.player, player.x, player.y, playerWidth, playerWidth);
		batch.end();
		
		if(Gdx.input.isTouched()) {
			Vector3 touchPos = new Vector3();
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			camera.unproject(touchPos);
			player.x = touchPos.x - (playerWidth/2);
		}

		backgroundControl();
		
		if(TimeUtils.nanoTime() - lastDropTime > spawnTime)
		{
			spawnEnemy();
		}
		
		if( ((bombsDropped + 1) * 10) == score)
		{
			dropBomb();	
		}
		
		Iterator<Enemy> iter = enemies.iterator();
		while(iter.hasNext())
		{		
			Enemy enemy = iter.next();
			enemy.y -=  (enemy.getSpeed() * Gdx.graphics.getDeltaTime()) + (int) enemyNumber/2;
			if(enemy.y + 48 < 0)
			{
				score++;
				if(level == 3)
				{
					spawnTime = spawnTime - 750000;
				}
				if(level == 2)
				{
					spawnTime = spawnTime - 500000;
				}
				if(level == 1)
				{
					spawnTime = spawnTime - 100000;
				}
				
				iter.remove();
			}
			if (Intersector.overlapCircles(enemy, player))
			{
				dispose();
				game.setScreen(new GameOverScreen(game,level, myRequestHandler, score));
				return;
			}
		}
		Iterator<Item> itemIterator = items.iterator();
		while(itemIterator.hasNext())
		{
			Item item = itemIterator.next();
			item.y -= (item.getSpeed() * Gdx.graphics.getDeltaTime());
			if(item.y + 48 < 0)
			{
				itemIterator.remove();
			}
			if (Intersector.overlapCircles(item, player))
			{
				enemies.clear();
				itemIterator.remove();
			}
		}
		
		if (score == 20)		
			//SwarmAchievement.unlock(11105);
		if (score == 120)	
			//SwarmAchievement.unlock(11107);
		if (score == 240)
			//SwarmAchievement.unlock(11109);
		if (score == 400)
			//SwarmAchievement.unlock(11111);
		
		batch.begin();
		
		
		for(Item bomb : items) {
			batch.draw(com.tatianitasmile.Assets.bomb, bomb.x, bomb.y,playerWidth,playerWidth);
		}
		
		for(Enemy enemy: enemies) {
			batch.draw(Assets.enemy, enemy.x, enemy.y,playerWidth,playerWidth);
		}
		batch.end();
		stage.act(delta);
		stage.draw();
		
		String lblText = "Score: " + score;
		lbl = new Label(lblText, Assets.skin);
		table.clear();
		table.add(lbl).bottom().left();
		table.setX(lbl.getWidth()/2 + 10);
		table.setY(screenHeight - 15);
		stage.addActor(table);
	}			

	private void backgroundControl()
	{
		int bgAssetWidth = screenWidth + (screenHeight/2); 
		if((backgroundXPosition+bgAssetWidth )> screenWidth && !changeDirection)
		{
			backgroundXPosition -= 0.5;
		}
		if ( (backgroundXPosition+bgAssetWidth) <= screenWidth && !changeDirection)
		{
			changeDirection = true;
		}

		if (backgroundXPosition< 0 && changeDirection)
		{
			backgroundXPosition += 0.5;
		}
		
		if (backgroundXPosition >= 0 && changeDirection)
		{
			changeDirection = false;
		}
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		stage = new Stage();
		table = new Table(Assets.skin);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		SoundManager.stopMusic();
		stage.clear();
		stage.dispose();
		batch.dispose();
	}

}
