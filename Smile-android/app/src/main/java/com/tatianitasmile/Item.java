package com.tatianitasmile;

import com.badlogic.gdx.math.Circle;

abstract class Item extends Circle {

	private static final long serialVersionUID = 4452941336424155849L;
	private int id;
	private float speed;
	private String name;
	
	public Item(float x, float y, float radius)
	{
		super(x,y,radius);
	}
	
	abstract void performAction();
	
	public float getSpeed()
	{
		return speed;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setSpeed(float speed)
	{
		this.speed = speed;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
}
