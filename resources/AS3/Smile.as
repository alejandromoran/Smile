﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.Event;
	
	public class Smile extends MovieClip {
		

		
		private var startScreen : Start;
		private var gameLogic : GameLogic;
		private var gameOver : GameOver;
		private var soundControl : SoundControl;
		private var scoreManager : ScoreManager;
		private var username : String;
		public function Smile() {
			// constructor code
			StartScreen();
			this.addEventListener(Event.ENTER_FRAME, EnterFrameHandler);
			
		}
		private function StartScreen() : void
		{
			startScreen = new Start(stage);
		}
		
		private function EnterFrameHandler(e:Event)
		{	
			if (startScreen != null && startScreen.StartToPlay())
			{
				username = startScreen.GetUsername();
				stage.removeChild(startScreen);
				startScreen = null;
				StartGame();
			}

			if (gameLogic != null && gameLogic.StopToPlay())
			{
				soundControl.Stop();
				StopGame();
			}
			if(gameOver != null && gameOver.PlayAgain())
			{
				stage.removeChild(gameOver);
				gameOver = null;
				StartGame();
			}
			
		}
		public function StartGame() : void
		{
			gameLogic = new GameLogic(stage);
			soundControl = new SoundControl("bso.mp3");
			soundControl.Play();
			
		}
		private function StopGame() : void
		{
			var score : uint = gameLogic.GetScore();
			gameLogic = null;
			gameOver = new GameOver(stage, score);
			scoreManager = new ScoreManager();
			scoreManager.SendScore(username, score);
		}
	}
	
}
