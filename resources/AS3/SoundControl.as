﻿package  {
	
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	
	public class SoundControl {
		
		private var sound : Sound;
		private var channel : SoundChannel;
		private var soundUrl : URLRequest;
		
		public function SoundControl(soundUrl) {
			// constructor code
			this.soundUrl = new URLRequest(soundUrl);
			sound = new Sound();
			channel = new SoundChannel();
		}
		public function Play()
		{
			var trans:SoundTransform = new SoundTransform(0.2,0);
			sound.load(soundUrl);
			channel = sound.play(0,10,trans);
		}
		public function Stop()
		{
			channel.stop();
		}

	}
	
}
